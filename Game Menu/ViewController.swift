//
//  ViewController.swift
//  Game Menu
//
//  Created by Michael Di Meo on 19/11/2019.
//  Copyright © 2019 Michael Di Meo. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation


class ViewController: UIViewController {
    
    let truth = "David is the best"
    
    var time = 0
    var timer = Timer()
    var audioPlayer: AVAudioPlayer!
    
    @IBOutlet weak var nameGame: UILabel!
    @IBOutlet weak var tapToStartButton: UIButton!
    @IBOutlet weak var madeBy: UIButton!
    @IBOutlet weak var wrenchButton: UIButton!
    @IBOutlet weak var menuGame: UIImageView!
    @IBOutlet weak var wrenchIcon: UIImageView!
    
//    Outlet Collection
    @IBOutlet var starsImage: [UIView]!
    @IBOutlet var starsImage2: [UIView]!
    @IBOutlet var starsImage3: [UIView]!
    
    
    func playSoundWith(fileName: String, fileExtension: String) -> Void{
        let audioSourceUrl: URL!
        audioSourceUrl = Bundle.main.url(forResource: fileName, withExtension: fileExtension)
        
        if audioSourceUrl == nil {
            print("Requested Song Cannot Be Played!")
        } else {
            do{
                audioPlayer = try AVAudioPlayer.init(contentsOf: audioSourceUrl)
                audioPlayer.prepareToPlay()
                audioPlayer.play()
                
            } catch {
                print(error)
            }
        }
        
    }
    
    
    
    
    func timerFunctionAppear(){
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ViewController.starBringing), userInfo: nil, repeats: true)
    }
        
    func timerFunctionDisappear(){
           timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ViewController.starBringing2), userInfo: nil, repeats: true)
       }
           
   func timerFunctionAppear2(){
          timer = Timer.scheduledTimer(timeInterval: 1.3, target: self, selector: #selector(ViewController.starBringing3), userInfo: nil, repeats: true)
      }
          
      func timerFunctionDisappear2(){
             timer = Timer.scheduledTimer(timeInterval: 1.3, target: self, selector: #selector(ViewController.starBringing4), userInfo: nil, repeats: true)
         }
    
    func timerFunctionAppear3(){
        timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(ViewController.starBringing5), userInfo: nil, repeats: true)
        }
            
        func timerFunctionDisappear3(){
            timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(ViewController.starBringing6), userInfo: nil, repeats: true)
           }
               
             
        
    
    
    
    
    
    
    
    
    @objc func starAppear(){
        for star in starsImage {
            star.alpha = 0.5
                star.alpha = 0.0
        }
    }

    @objc func starDisappear(){
        for star in starsImage {
                star.alpha = 0.0
            star.alpha = 0.5
        }
    }
    
    @objc func starAppear2(){
        for star in starsImage2 {
            star.alpha = 0.5
                star.alpha = 0.0
        }
    }

    @objc func starDisappear2(){
        for star in starsImage2 {
            star.alpha = 0.0
            star.alpha = 0.5
        }
    }
    
    @objc func starAppear3(){
          for star in starsImage3 {
              star.alpha = 0.5
                  star.alpha = 0.0
          }
      }

      @objc func starDisappear3(){
          for star in starsImage3 {
              star.alpha = 0.0
              star.alpha = 0.5
          }
      }
    
    
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playSoundWith(fileName: "Game Menu Music", fileExtension: "mp3")
        
        

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        timerFunctionAppear()
        timerFunctionDisappear()
        timerFunctionAppear2()
        timerFunctionDisappear2()
        timerFunctionAppear3()
        timerFunctionDisappear3()
    }
    

    
    
    
    @IBAction func tapToStart(_ sender: UIButton) {
        print("Button Was Tapped!")
        nameGame.isHidden = true
        tapToStartButton.isHidden = true
        madeBy.isHidden = true
        wrenchIcon.isHidden = true
        wrenchButton.isHidden = true
        audioPlayer.stop()
        
        UIView.animate(withDuration: 2.0){
            self.menuGame.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }
        
        
    }
    
    @IBAction func tapToCredits(_ sender: UIButton) {
        print("Button Was Tapped!")
        timer.invalidate()
        }
    
    @IBAction func tapToSettings(_ sender: UIButton) {
        print("Button Was Tapped!")
        timer.invalidate()
        }
    
    
    
    
    
    
    
    @objc func starBringing(){
        if starsImage != nil{
        for star in starsImage{
            if star != nil {
                UIView.animate(withDuration: 0.8){
                    self.starDisappear()
                }
                UIView.animate(withDuration: 0.8){
                    self.starAppear()
                }
              }
            }
        }
    }
    
    
    @objc func starBringing2(){
        if starsImage != nil{
            for star in starsImage{
                    if star != nil {
                        UIView.animate(withDuration: 0.8){
                            self.starAppear()
                        }
                        UIView.animate(withDuration: 0.8){
                            self.starDisappear()
                        }
                    
                    }
                }
            }
    }

    @objc func starBringing3(){
        if starsImage2 != nil{
        for star in starsImage2{
            if star != nil {
                UIView.animate(withDuration: 1.2){
                    self.starDisappear2()
                }
                UIView.animate(withDuration: 1.2){
                    self.starAppear2()
                }
            
            }
        }
    }
}
    
    @objc func starBringing4(){
        if starsImage2 != nil{
            for star in starsImage2{
                    if star != nil {
                        UIView.animate(withDuration: 1.2){
                            self.starAppear2()
                        }
                        UIView.animate(withDuration: 1.2){
                            self.starDisappear2()
                        }
                    
                    }
                }
            }
    }
   
     @objc func starBringing5(){
            if starsImage3 != nil{
            for star in starsImage2{
                if star != nil {
                    UIView.animate(withDuration: 1.6){
                        self.starDisappear3()
                    }
                    UIView.animate(withDuration: 1.6){
                        self.starAppear3()
                    }
                
                }
            }
        }
    }
        
        @objc func starBringing6(){
            if starsImage3 != nil{
                for star in starsImage2{
                        if star != nil {
                            UIView.animate(withDuration: 1.6){
                                self.starAppear3()
                            }
                            UIView.animate(withDuration: 1.6){
                                self.starDisappear3()
                            }
                        
                        }
                    }
                }
        }
    

    
}



    
    



